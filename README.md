**********OCBC Pay**********
You can import this project directly by using AndroidStudio.
The version I used was 4.1.

Important Note :
When you launch the app, the user Bob is not created, but it shows in the username
Textbox in the Login page. Since user has not been created, it will ask you to create one.
Once you go to the Registration page (user name and password already set in the Textboxes),press register, then it will create the user.
Then you comeback to the Login page and then press LOGIN button.

----------------Assumptions---------------
1. Assumption I made was, the users will use latest devices because
   I used  "minSdkVersion 21".

2. User will not use bigger amounts.

---------------Enhancements---------------
1. Password is being saved in clear text, but it can be saved as hashcode (SHA256 or 512)       with padding. To do that, we can use bouncycastle library, and then do encrypt (symetric) encryption. The key can be created dynamically using salt.

2. The debt amount can be shown in a UI label.