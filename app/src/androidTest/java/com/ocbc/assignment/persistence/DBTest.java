package com.ocbc.assignment.persistence;

import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import com.ocbc.assignment.model.User;
import com.ocbc.assignment.utils.AppExecutors;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class DBTest {
    private UserInfoDAO userDao;
    private UserInfoDatabase db;
    private Context appContext;

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        db = Room.inMemoryDatabaseBuilder(context, UserInfoDatabase.class).build();
        userDao = db.getUserInfoDAO();

        appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    @Test
    public void insertUserTest1() throws Exception {
        User user = new User();
        user.setUserName("Bob");
        user.setAmount(10.0);

        userDao.insertUser(user);
        User userDaoUser = userDao.getUser("Bob");
        assertTrue("Bob".equals(userDaoUser.getUserName()));
    }

    @Test
    public void insertUserTest2() throws Exception {
        User user = new User();
        user.setUserName("Bob");
        user.setAmount(10.0);

        userDao.insertUser(user);
        User userDaoUser = userDao.getUser("Bob");
        assertTrue("Bob".equals(userDaoUser.getUserName()));

        try {
            userDao.insertUser(user);
        } catch (Exception e) {
            assertTrue(e instanceof SQLiteConstraintException);
        }
    }

    @Test
    public void insertUserTest3() throws Exception {
        User user = new User();
        user.setUserName("Bob");
        user.setAmount(10.0);

        User user1 = new User();
        user1.setUserName("Alice");
        user1.setAmount(100.0);

        userDao.insertUser(user);
        userDao.insertUser(user1);
        LiveData<List<User>> userDaoUser = userDao.getRecipients();
        List<User> userList = getLivedataInfo(userDaoUser);
        assertTrue("Bob".equals(userList.get(0).getUserName()));
    }

    @Test
    public void insertUserTest4() throws Exception {
        User user = new User();
        user.setUserName("Bob");
        user.setAmount(10.0);

        userDao.insertUser(user);
        User userDaoUser = userDao.getUser("Bob");
        assertTrue("Bob".equals(userDaoUser.getUserName()));

        userDao.update(5.0, 0.0, 0.0, "Bob");
        User userDaoUser1 = userDao.getUser("Bob");
        assertTrue(userDaoUser1.getAmount() == 5.0);
    }


    public static <T> T getLivedataInfo(final LiveData<T> liveData) throws InterruptedException {
        final Object[] data = new Object[1];
        final CountDownLatch latch = new CountDownLatch(1);
        Observer<T> observer = new Observer<T>() {
            @Override
            public void onChanged(@Nullable T o) {
                data[0] = o;
                latch.countDown();
                liveData.removeObserver(this);
            }
        };
        new AppExecutors().mainThread().execute(() ->
                liveData.observeForever(observer));
        if (!latch.await(2, TimeUnit.SECONDS)) {
            throw new RuntimeException("LiveData value was never set.");
        }
        return (T) data[0];
    }
}
