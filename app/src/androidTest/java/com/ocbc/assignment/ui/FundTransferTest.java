package com.ocbc.assignment.ui;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.core.app.ActivityScenario;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.ViewInteraction;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;

import com.ocbc.assignment.R;
import com.ocbc.assignment.repository.Repository;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class FundTransferTest {

    @Before
    public void initialSetup() {
        ActivityScenario activityScenario = ActivityScenario.launch(LoginActivity.class);
        activityScenario.onActivity((ActivityScenario.ActivityAction<LoginActivity>) activity -> {
        });
    }

    @Test
    public void fundTransferTest2() {
        ViewInteraction materialButton = onView(
                allOf(withId(R.id.btnlogin), withText("Login"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                0),
                        isDisplayed()));
        materialButton.perform(click());

        ViewInteraction materialButton2 = onView(
                allOf(withId(android.R.id.button1), withText("Ok"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.buttonPanel),
                                        0),
                                3)));
        materialButton2.perform(scrollTo(), click());

        ViewInteraction materialButton3 = onView(
                allOf(withId(R.id.btnRegister), withText("Register"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                0),
                        isDisplayed()));
        materialButton3.perform(click());

        ViewInteraction materialButton4 = onView(
                allOf(withId(android.R.id.button1), withText("Ok"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.buttonPanel),
                                        0),
                                3)));
        materialButton4.perform(scrollTo(), click());

        ViewInteraction materialButton5 = onView(
                allOf(withId(R.id.btnlogin), withText("Login"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                0),
                        isDisplayed()));
        materialButton5.perform(click());

        ViewInteraction textView = onView(
                allOf(withId(R.id.user_name_txt), withText("Hello, Bob!"),
                        withParent(withParent(withId(R.id.topup_view))),
                        isDisplayed()));
        textView.check(matches(withText("Hello, Bob!")));

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.topup_amount),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.topup_view),
                                        0),
                                1),
                        isDisplayed()));
        appCompatEditText.perform(replaceText("80"), closeSoftKeyboard());

        ViewInteraction materialButton6 = onView(
                allOf(withId(R.id.topup_btn), withText("Topup"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.topup_view),
                                        0),
                                0),
                        isDisplayed()));
        materialButton6.perform(click());

        ViewInteraction textView2 = onView(
                allOf(withId(R.id.currentblance_txt), withText("80.0"),
                        withParent(withParent(withId(R.id.topup_view))),
                        isDisplayed()));
        textView2.check(matches(withText("80.0")));

        ViewInteraction appCompatImageButton = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                withClassName(is("com.google.android.material.appbar.AppBarLayout")),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatImageButton.perform(click());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.username), withText("Bob"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                3),
                        isDisplayed()));
        appCompatEditText2.perform(replaceText("Alice"));

        waitFor(3000);

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.username), withText("Alice"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                3),
                        isDisplayed()));
        appCompatEditText3.perform(closeSoftKeyboard());

        ViewInteraction materialButton7 = onView(
                allOf(withId(R.id.btnlogin), withText("Login"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                0),
                        isDisplayed()));
        materialButton7.perform(click());

        //waitFor(3000);
        // myExecutor.call(()->onView(allOf(withId(R.id.title),isDisplayed())));

        ViewInteraction textView3 = onView(
                allOf(withId(R.id.user_name_txt), withText("Hello, Alice!"),
                        withParent(withParent(withId(R.id.topup_view))),
                        isDisplayed()));
        textView3.check(matches(withText("Hello, Alice!")));

        waitFor(3000);

        ViewInteraction textView4 = onView(
                allOf(withId(R.id.currentblance_txt), withText("0.0"),
                        withParent(withParent(withId(R.id.topup_view))),
                        isDisplayed()));
        textView4.check(matches(withText("0.0")));

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.topup_amount),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.topup_view),
                                        0),
                                1),
                        isDisplayed()));
        appCompatEditText4.perform(replaceText("100"), closeSoftKeyboard());

        ViewInteraction materialButton8 = onView(
                allOf(withId(R.id.topup_btn), withText("Topup"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.topup_view),
                                        0),
                                0),
                        isDisplayed()));
        materialButton8.perform(click());

        ViewInteraction textView5 = onView(
                allOf(withId(R.id.currentblance_txt), withText("100.0"),
                        withParent(withParent(withId(R.id.topup_view))),
                        isDisplayed()));
        textView5.check(matches(withText("100.0")));

        ViewInteraction appCompatImageButton2 = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                withClassName(is("com.google.android.material.appbar.AppBarLayout")),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatImageButton2.perform(click());

        ViewInteraction textView6 = onView(
                allOf(withId(R.id.logo), withText("Login"),
                        withParent(withParent(withId(android.R.id.content))),
                        isDisplayed()));
        textView6.check(matches(withText("Login")));

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.username), withText("Alice"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                3),
                        isDisplayed()));
        appCompatEditText5.perform(click());

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.username), withText("Alice"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                3),
                        isDisplayed()));
        appCompatEditText6.perform(replaceText("Bob"));

        ViewInteraction appCompatEditText7 = onView(
                allOf(withId(R.id.username), withText("Bob"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                3),
                        isDisplayed()));
        appCompatEditText7.perform(closeSoftKeyboard());

        ViewInteraction materialButton9 = onView(
                allOf(withId(R.id.btnlogin), withText("Login"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                0),
                        isDisplayed()));
        materialButton9.perform(click());

        ViewInteraction textView7 = onView(
                allOf(withId(R.id.currentblance_txt), withText("80.0"),
                        withParent(withParent(withId(R.id.topup_view))),
                        isDisplayed()));
        textView7.check(matches(withText("80.0")));

        ViewInteraction textView8 = onView(
                allOf(withId(R.id.user_name_txt), withText("Hello, Bob!"),
                        withParent(withParent(withId(R.id.topup_view))),
                        isDisplayed()));
        textView8.check(matches(withText("Hello, Bob!")));

        ViewInteraction appCompatEditText8 = onView(
                allOf(withId(R.id.transfer_amount),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.cardview.widget.CardView")),
                                        0),
                                1),
                        isDisplayed()));
        appCompatEditText8.perform(replaceText("50"), closeSoftKeyboard());

        pressBack();

        waitFor(4000);

        ViewInteraction materialButton99 = onView(
                allOf(withId(R.id.btnlogin), withText("Login"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                0),
                        isDisplayed()));
        materialButton99.perform(click());

        ViewInteraction appCompatEditText155 = onView(
                allOf(withId(R.id.transfer_amount),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.cardview.widget.CardView")),
                                        0),
                                1),
                        isDisplayed()));
        appCompatEditText155.perform(replaceText("50"), closeSoftKeyboard());


        ViewInteraction materialButton10 = onView(
                allOf(withId(R.id.transfer_btn), withText("Transfer"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.cardview.widget.CardView")),
                                        0),
                                0),
                        isDisplayed()));
        materialButton10.perform(click());

        //-----
        ViewInteraction textView9 = onView(
                allOf(withId(R.id.currentblance_txt), withText("30.0"),
                        withParent(withParent(withId(R.id.topup_view))),
                        isDisplayed()));
        textView9.check(matches(withText("30.0")));

        ViewInteraction appCompatEditText9 = onView(
                allOf(withId(R.id.transfer_amount),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.cardview.widget.CardView")),
                                        0),
                                1),
                        isDisplayed()));
        appCompatEditText9.perform(click());

        ViewInteraction appCompatEditText10 = onView(
                allOf(withId(R.id.transfer_amount),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.cardview.widget.CardView")),
                                        0),
                                1),
                        isDisplayed()));
        appCompatEditText10.perform(replaceText("100"), closeSoftKeyboard());

        pressBack();

        ViewInteraction materialButton999 = onView(
                allOf(withId(R.id.btnlogin), withText("Login"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                0),
                        isDisplayed()));
        materialButton999.perform(click());

        ViewInteraction appCompatEditText100 = onView(
                allOf(withId(R.id.transfer_amount),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.cardview.widget.CardView")),
                                        0),
                                1),
                        isDisplayed()));
        appCompatEditText100.perform(replaceText("100"), closeSoftKeyboard());

        closeSoftKeyboard();

        ViewInteraction materialButton11 = onView(
                allOf(withId(R.id.transfer_btn), withText("Transfer"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.cardview.widget.CardView")),
                                        0),
                                0),
                        isDisplayed()));
        materialButton11.perform(click());


        ViewInteraction textView10 = onView(
                allOf(withId(R.id.currentblance_txt), withText("0.0"),
                        withParent(withParent(withId(R.id.topup_view))),
                        isDisplayed()));
        textView10.check(matches(withText("0.0")));

        ViewInteraction appCompatEditText11 = onView(
                allOf(withId(R.id.topup_amount),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.topup_view),
                                        0),
                                1),
                        isDisplayed()));
        appCompatEditText11.perform(replaceText("30"), closeSoftKeyboard());

        ViewInteraction materialButton12 = onView(
                allOf(withId(R.id.topup_btn), withText("Topup"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.topup_view),
                                        0),
                                0),
                        isDisplayed()));
        materialButton12.perform(click());

        ViewInteraction textView11 = onView(
                allOf(withId(R.id.currentblance_txt), withText("0.0"),
                        withParent(withParent(withId(R.id.topup_view))),
                        isDisplayed()));
        textView11.check(matches(withText("0.0")));

        ViewInteraction appCompatImageButton3 = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                withClassName(is("com.google.android.material.appbar.AppBarLayout")),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatImageButton3.perform(click());

        ViewInteraction textView12 = onView(
                allOf(withId(R.id.logo), withText("Login"),
                        withParent(withParent(withId(android.R.id.content))),
                        isDisplayed()));
        textView12.check(matches(withText("Login")));

        ViewInteraction appCompatEditText12 = onView(
                allOf(withId(R.id.username), withText("Bob"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                3),
                        isDisplayed()));
        appCompatEditText12.perform(click());

        ViewInteraction appCompatEditText13 = onView(
                allOf(withId(R.id.username), withText("Bob"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                3),
                        isDisplayed()));
        appCompatEditText13.perform(replaceText("Alice"));

        ViewInteraction appCompatEditText14 = onView(
                allOf(withId(R.id.username), withText("Alice"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                3),
                        isDisplayed()));
        appCompatEditText14.perform(closeSoftKeyboard());

        ViewInteraction materialButton13 = onView(
                allOf(withId(R.id.btnlogin), withText("Login"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                0),
                        isDisplayed()));
        materialButton13.perform(click());

        ViewInteraction textView13 = onView(
                allOf(withId(R.id.user_name_txt), withText("Hello, Alice!"),
                        withParent(withParent(withId(R.id.topup_view))),
                        isDisplayed()));
        textView13.check(matches(withText("Hello, Alice!")));

        ViewInteraction textView14 = onView(
                allOf(withId(R.id.currentblance_txt), withText("210.0"),
                        withParent(withParent(withId(R.id.topup_view))),
                        isDisplayed()));
        textView14.check(matches(withText("210.0")));

        ViewInteraction appCompatEditText15 = onView(
                allOf(withId(R.id.transfer_amount),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.cardview.widget.CardView")),
                                        0),
                                1),
                        isDisplayed()));
        appCompatEditText15.perform(replaceText("30"), closeSoftKeyboard());

        closeSoftKeyboard();

        ViewInteraction materialButton14 = onView(
                allOf(withId(R.id.transfer_btn), withText("Transfer"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.cardview.widget.CardView")),
                                        0),
                                0),
                        isDisplayed()));
        materialButton14.perform(click());

        ViewInteraction textView15 = onView(
                allOf(withId(R.id.currentblance_txt), withText("210.0"),
                        withParent(withParent(withId(R.id.topup_view))),
                        isDisplayed()));
        textView15.check(matches(withText("210.0")));

        ViewInteraction appCompatImageButton4 = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                withClassName(is("com.google.android.material.appbar.AppBarLayout")),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatImageButton4.perform(click());

        ViewInteraction appCompatEditText16 = onView(
                allOf(withId(R.id.username), withText("Alice"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                3),
                        isDisplayed()));
        appCompatEditText16.perform(click());

        ViewInteraction textView16 = onView(
                allOf(withId(R.id.logo), withText("Login"),
                        withParent(withParent(withId(android.R.id.content))),
                        isDisplayed()));
        textView16.check(matches(withText("Login")));

        ViewInteraction appCompatEditText17 = onView(
                allOf(withId(R.id.username), withText("Alice"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                3),
                        isDisplayed()));
        appCompatEditText17.perform(replaceText("Bob"));

        ViewInteraction appCompatEditText18 = onView(
                allOf(withId(R.id.username), withText("Bob"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                3),
                        isDisplayed()));
        appCompatEditText18.perform(closeSoftKeyboard());

        ViewInteraction materialButton15 = onView(
                allOf(withId(R.id.btnlogin), withText("Login"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                0),
                        isDisplayed()));
        materialButton15.perform(click());

        ViewInteraction textView17 = onView(
                allOf(withId(R.id.user_name_txt), withText("Hello, Bob!"),
                        withParent(withParent(withId(R.id.topup_view))),
                        isDisplayed()));
        textView17.check(matches(withText("Hello, Bob!")));

        ViewInteraction textView18 = onView(
                allOf(withId(R.id.currentblance_txt), withText("0.0"),
                        withParent(withParent(withId(R.id.topup_view))),
                        isDisplayed()));
        textView18.check(matches(withText("0.0")));

        ViewInteraction appCompatEditText19 = onView(
                allOf(withId(R.id.topup_amount),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.topup_view),
                                        0),
                                1),
                        isDisplayed()));
        appCompatEditText19.perform(replaceText("100"), closeSoftKeyboard());

        ViewInteraction materialButton16 = onView(
                allOf(withId(R.id.topup_btn), withText("Topup"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.topup_view),
                                        0),
                                0),
                        isDisplayed()));
        materialButton16.perform(click());

        ViewInteraction textView19 = onView(
                allOf(withId(R.id.currentblance_txt), withText("90.0"),
                        withParent(withParent(withId(R.id.topup_view))),
                        isDisplayed()));
        textView19.check(matches(withText("90.0")));
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }

    public static ViewAction waitFor(long delay) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return ViewMatchers.isRoot();
            }

            @Override
            public String getDescription() {
                return "wait for " + delay + "milliseconds";
            }

            @Override
            public void perform(UiController uiController, View view) {
                uiController.loopMainThreadForAtLeast(delay);
            }
        };
    }

    @AfterClass
    public static void tearDown() {
        Context context = ApplicationProvider.getApplicationContext();
        Repository.getInstance(context).deleteAll();
    }
}
