package com.ocbc.assignment.utils;


import android.content.Context;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertTrue;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class CommonUtilsTest {

    Context appContext;
    String usernameError = "Username cannot be empty!";
    String pwdError = "Password cannot be empty!";
    String pwdMismatchError = "Passwords are not identical. Please try again.";


    @Before
    public void registerIdlingResource() {
        appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
    }


    @Test
    public void validateFieldsTest1() {
        String user = "user";
        String pwd = "pwd";

        String out = CommonUtils.getInstance().validateFields(appContext, user, pwd);
        assertTrue(out.length() == 0);
    }

    @Test
    public void validateFieldsPwdEmptyTest2() {
        String user = "user";
        String pwd = "";

        String out = CommonUtils.getInstance().validateFields(appContext, user, pwd);
        assertTrue(out.length() != 0);
        assertTrue(out.equals(pwdError));
    }

    @Test
    public void validateFieldsUserNameEmptyTest3() {
        String user = "";
        String pwd = "pwd";

        String out = CommonUtils.getInstance().validateFields(appContext, user, pwd);
        assertTrue(out.length() != 0);
        assertTrue(out.equals(usernameError));
    }

    @Test
    public void validateFieldsTestPwdNullTest4() {
        String user = "user";
        String pwd = null;

        String out = CommonUtils.getInstance().validateFields(appContext, user, pwd);
        assertTrue(out.length() != 0);
        assertTrue(out.equals(pwdError));
    }

    @Test
    public void validateFieldsUserNameNullTest5() {
        String user = null;
        String pwd = "pwd";

        String out = CommonUtils.getInstance().validateFields(appContext, user, pwd);
        assertTrue(out.length() != 0);
        assertTrue(out.equals(usernameError));
    }

    @Test
    public void validateRegistrationFormTest1() {
        String user = "user";
        String pwd1 = "pwd1";
        String pwd2 = "pwd1";

        String out = CommonUtils.getInstance().validateRegistrationForm(appContext, user, pwd1, pwd2);
        assertTrue(out.equals(""));
    }

    @Test
    public void validateRegistrationFormTest2() {
        String user = "";
        String pwd1 = "pwd1";
        String pwd2 = "pwd2";

        String out = CommonUtils.getInstance().validateRegistrationForm(appContext, user, pwd1, pwd2);
        assertTrue(out.equals(usernameError));
    }

    @Test
    public void validateRegistrationFormTest3() {
        String user = null;
        String pwd1 = "pwd1";
        String pwd2 = "pwd2";

        String out = CommonUtils.getInstance().validateRegistrationForm(appContext, user, pwd1, pwd2);
        assertTrue(out.equals(usernameError));
    }

    @Test
    public void validateRegistrationFormTest4() {
        String user = "user";
        String pwd1 = "";
        String pwd2 = "pwd2";

        String out = CommonUtils.getInstance().validateRegistrationForm(appContext, user, pwd1, pwd2);
        assertTrue(out.equals(pwdError));
    }

    @Test
    public void validateRegistrationFormTest5() {
        String user = "user";
        String pwd1 = null;
        String pwd2 = "pwd2";

        String out = CommonUtils.getInstance().validateRegistrationForm(appContext, user, pwd1, pwd2);
        assertTrue(out.equals(pwdError));
    }

    @Test
    public void validateRegistrationFormTest6() {
        String user = "user";
        String pwd1 = "pwd1";
        String pwd2 = "";

        String out = CommonUtils.getInstance().validateRegistrationForm(appContext, user, pwd1, pwd2);
        assertTrue(out.equals(pwdError));
    }

    @Test
    public void validateRegistrationFormTest7() {
        String user = "user";
        String pwd1 = "pwd1";
        String pwd2 = null;

        String out = CommonUtils.getInstance().validateRegistrationForm(appContext, user, pwd1, pwd2);
        assertTrue(out.equals(pwdError));
    }

    @Test
    public void validateRegistrationFormTest8() {
        String user = "user";
        String pwd1 = "pwd1";
        String pwd2 = "pwd";

        String out = CommonUtils.getInstance().validateRegistrationForm(appContext, user, pwd1, pwd2);
        assertTrue(out.equals(pwdMismatchError));
    }

    @Test
    public void validateWelcomeMessageTest1() {
        String user = "Bob";
        String out = CommonUtils.getInstance().setWelcomeMessage(appContext, user);
        assertTrue(out.equals("Hello, Bob!"));
    }

    @Test
    public void validateWelcomeMessageTest2() {
        String user = "";
        String out = CommonUtils.getInstance().setWelcomeMessage(appContext, user);
        assertTrue(out.equals("Hello,"));
    }

    @Test
    public void validateWelcomeMessageTest3() {
        String user = null;
        String out = CommonUtils.getInstance().setWelcomeMessage(appContext, user);
        assertTrue(out.equals("Hello,"));
    }



}
