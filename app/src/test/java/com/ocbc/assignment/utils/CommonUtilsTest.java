package com.ocbc.assignment.utils;


import com.ocbc.assignment.model.User;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CommonUtilsTest {

    List<User> users = new ArrayList<>();

    @Before
    public void setup() {
        User user1 = new User();
        user1.setUserName("user1");

        User user2 = new User();
        user2.setUserName("user2");

        User user3 = new User();
        user3.setUserName("user3");

        User user4 = new User();
        user4.setUserName("user4");

        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
    }

    @Test
    public void validateGetInstance() {
        Object utils = CommonUtils.getInstance();
        assertTrue(utils instanceof CommonUtils);
    }

    @Test
    public void validatePasswordTest1() {

        String pwd1 = "password";
        String pwd2 = "password";

        boolean out = CommonUtils.getInstance().isPasswordIdentical(pwd1, pwd2);
        assertTrue(out);
    }

    @Test
    public void validatePasswordTest2() {

        String pwd1 = "password1";
        String pwd2 = "password";

        boolean out = CommonUtils.getInstance().isPasswordIdentical(pwd1, pwd2);
        assertFalse(out);
    }

    @Test
    public void roundAmountTest1() {
        double input = 1.234;
        double out = CommonUtils.getInstance().roundAmount(input);
        assertTrue(out == 1.23);
    }

    @Test
    public void roundAmountTest2() {
        double input = 1.579;
        double out = CommonUtils.getInstance().roundAmount(input);
        assertTrue(out == 1.58);
    }

    @Test
    public void roundAmountTest3() {
        double input = 0.0;
        double out = CommonUtils.getInstance().roundAmount(input);
        assertTrue(out == 0.0);
    }

    @Test
    public void validateFieldsTest1() {

        String input = "input";
        boolean out = CommonUtils.getInstance().validateFields(input);
        assertTrue(out);
    }

    @Test
    public void validateFieldsEmptyTest() {
        String input = "";
        boolean out = CommonUtils.getInstance().validateFields(input);
        assertFalse(out);
    }

    @Test
    public void validateFieldsNullTest() {
        String input = null;
        boolean out = CommonUtils.getInstance().validateFields(input);
        assertFalse(out);
    }
}
