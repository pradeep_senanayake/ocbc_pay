package com.ocbc.assignment.persistence;


import com.google.gson.Gson;
import com.ocbc.assignment.model.User;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;


import static org.junit.Assert.assertTrue;
import static org.powermock.api.mockito.PowerMockito.mock;

@RunWith(PowerMockRunner.class)
public class DataConverterTest {

    @Test
    public void toUserTest3() {
        String input = new Gson().toJson(new User());
        DataConverter dataConverter = new DataConverter();
        Object output = dataConverter.toUser(input);
        assertTrue(output instanceof User);
    }

    @Test
    public void toUserTest4() {
        String input = null;
        DataConverter dataConverter = new DataConverter();
        Object output = dataConverter.toUser(input);
        assertTrue(output == null);
    }

    @Test
    public void toUserTest5() {
        String input = "";
        DataConverter dataConverter = new DataConverter();
        Object output = dataConverter.toUser(input);
        assertTrue(output == null);
    }

    @Test
    public void fromUserTest1() {
        User user = mock(User.class);
        DataConverter dataConverter = new DataConverter();
        Object output = dataConverter.fromUser(user);
        assertTrue(output instanceof String);
    }

    @Test
    public void fromUser2() {
        User user = null;
        DataConverter dataConverter = new DataConverter();
        Object output = dataConverter.fromUser(user);
        assertTrue(output == null);
    }
}


