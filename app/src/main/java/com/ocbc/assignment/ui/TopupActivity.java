package com.ocbc.assignment.ui;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.LiveData;

import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.ocbc.assignment.R;
import com.ocbc.assignment.application.OCBCPaymentApplication;
import com.ocbc.assignment.databinding.ActivityTopupBinding;
import com.ocbc.assignment.model.User;
import com.ocbc.assignment.utils.CommonUtils;
import com.ocbc.assignment.utils.Constants;
import com.ocbc.assignment.utils.CustomDialogInterface;
import com.ocbc.assignment.utils.UiUtils;
import com.ocbc.assignment.utils.UpdateListener;

import java.util.List;

public class TopupActivity extends BaseActivity {

    private static final String TAG = Constants.PAYTAG;
    public static final String COMMON_TAG = TopupActivity.class.getSimpleName();
    private ActivityTopupBinding activityTopupBinding = null;
    private User recipient;
    private String userName;
    private String selectedRecipientID;
    private String[] userArray;
    private double currentAmount = 0.0;
    private double debtAmount = 0.0;
    private double transferAmount;
    private double debt;
    private double currentValue;
    private double payeeDept;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topup);
        initUI();
    }

    @Override
    void initViewModel() {
        super.initViewModel();
    }

    @Override
    void initUI() {
        activityTopupBinding = ActivityTopupBinding.inflate(getLayoutInflater());
        View view = activityTopupBinding.getRoot();
        setContentView(view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setActionbar();
        setTitle();
        initViewModel();
        initThreads();
        topupAction();
        setSpinnerDebtAmount();
        setInitData();
        performTransfer();
        Log.d(TAG, "Hello " + userName + "!");
        Log.d(TAG, "Your balance is " + currentAmount);
    }

    private void setActionbar() {
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setHomeButtonEnabled(true);
            actionbar.setDisplayShowHomeEnabled(true);
            actionbar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void setTitle() {
        setTitle(getString(R.string.topup_title));
    }

    private void setInitData() {
        OCBCPaymentApplication application = CommonUtils.getInstance()
                .getOCBCApplication(TopupActivity.this);
        userName = application.getName();
        currentAmount = application.getCurrentAmount();
        debtAmount = application.getDebt();
        String welcomeMsg = CommonUtils.getInstance()
                .setWelcomeMessage(TopupActivity.this, userName);
        activityTopupBinding.includedLayout.userNameTxt.setText(welcomeMsg);
        activityTopupBinding.includedLayout.currentbalanceLbl.setText(getString(R.string.currentblc_lbl));
        activityTopupBinding.includedLayout
                .currentblanceTxt.setText(String.valueOf(currentAmount));
    }

    private void setDeptAmount() {
        getUser(userArray[0], user -> {
            double recipientDept = user.getDebpt();
            Log.d(TAG, "Owing " + recipientDept + " from " + userArray[0]);
        });

        getUser(userName, user -> Log.d(TAG,
                "Owing " + user.getDebpt() + " to " + userArray[0]));
    }

    private void setSpinnerDebtAmount() {
        LiveData<List<User>> recipientList = userInfoViewModel.getRecepients();
        recipientList.observe(this, users -> {
            if (users != null) {
                userArray = CommonUtils.getInstance().getStringArray(users, userName);
                setDeptAmount();
                Log.d(COMMON_TAG, "recipientList: " + userArray.length);
                if (userArray != null && userArray.length != 0) {
                    ArrayAdapter adapter = new ArrayAdapter(TopupActivity.this,
                            android.R.layout.simple_spinner_item, userArray);
                    adapter.setDropDownViewResource(
                            android.R.layout.simple_spinner_dropdown_item);
                    activityTopupBinding.includedLayout.spinner.setAdapter(adapter);
                } else {
                    showAmountValidationMessage(getString(R.string.recipient_error),
                            getString(R.string.recipient_error_msg));
                }
            }
        });
        activityTopupBinding.includedLayout.spinner
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        selectedRecipientID = userArray[position];
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        selectedRecipientID = userArray[0];
                    }
                });
    }

    private void performTransfer() {

        activityTopupBinding.includedLayout.transferBtn
                .setOnClickListener(v -> {
                    Log.d(COMMON_TAG, "onClick: Transfer...");
                    String amount = activityTopupBinding.includedLayout
                            .transferAmount.getText().toString();
                    if (TextUtils.isEmpty(amount)) {
                        showAmountValidationMessage(getString(R.string.invalid_amount),
                                getString(R.string.invalid_amount_msg));
                    } else {
                        transferAmount = Double.parseDouble(amount);
                        Log.d(COMMON_TAG, "Transfer Amount " + amount);
                        if (transferAmount == 0) {
                            showAmountValidationMessage(getString(R.string.invalid_amount),
                                    getString(R.string.invalid_amount_msg));
                        } else {
                            String currentAmount = activityTopupBinding.includedLayout
                                    .currentblanceTxt.getText().toString();
                            currentValue = Double.parseDouble(currentAmount);

                            getUser(selectedRecipientID, user -> {
                                payeeDept = user.getDebpt();

                                if (payeeDept > 0) {
                                    if (transferAmount > payeeDept) {
                                        updateRecord(transferAmount - payeeDept,
                                                0.0, 0.0, selectedRecipientID);
                                        Log.d(TAG, "Pay " + selectedRecipientID
                                                + " " + transferAmount);
                                        Log.d(TAG, "Transferred "
                                                + (transferAmount - payeeDept)
                                                + " to " + selectedRecipientID);
                                        Log.d(TAG, "Your balance is " + 0.0);
                                        Log.d(TAG, "Owing " + 0.0 + " to "
                                                + selectedRecipientID);
                                    } else {
                                        updateRecord(0.0,
                                                payeeDept - transferAmount,
                                                0.0, selectedRecipientID);
                                        getUser(userName, new UpdateListener() {
                                            @Override
                                            public void getUser(User user) {
                                                if (user.getDebpt() == 0) {
                                                    Log.d(TAG, "Pay "
                                                            + selectedRecipientID + " "
                                                            + transferAmount);
                                                    Log.d(TAG, "Owing "
                                                            + (payeeDept - transferAmount)
                                                            + " from " + selectedRecipientID);
                                                    Log.d(TAG, "Your balance is "
                                                            + user.getAmount());
                                                } else {
                                                    Log.d(TAG, "Pay "
                                                            + selectedRecipientID + " "
                                                            + transferAmount);
                                                    Log.d(TAG, "Transferred "
                                                            + (payeeDept - transferAmount)
                                                            + " to " + selectedRecipientID);
                                                    Log.d(TAG, "Your balance is " + 0.0);
                                                    Log.d(TAG, "Owing "
                                                            + (payeeDept - transferAmount)
                                                            + " to " + selectedRecipientID);
                                                }
                                            }
                                        });

                                    }
                                } else {
                                    if (transferAmount < currentValue) {
                                        currentValue = currentValue - transferAmount;
                                        updateRecord(currentValue, 0,
                                                0, userName);
                                        currentValue = currentValue;
                                        updateRecipient(transferAmount);
                                        Log.d(TAG, "Pay " + selectedRecipientID
                                                + " " + transferAmount);
                                        Log.d(TAG, "Transferred " + (transferAmount)
                                                + " to " + selectedRecipientID);
                                        Log.d(TAG, "Your balance is " + currentValue);
                                        Log.d(TAG, "Owing " + 0.0 + " to "
                                                + selectedRecipientID);

                                    } else {
                                        debt = transferAmount - currentValue;
                                        updateRecord(0.0, debt, 0, userName);
                                        updateRecipient(currentValue);
                                        Log.d(TAG, "Pay " + selectedRecipientID + " "
                                                + transferAmount);
                                        Log.d(TAG, "Transferred " + (currentValue) +
                                                " to " + selectedRecipientID);
                                        Log.d(TAG, "Your balance is " + 0.0);
                                        Log.d(TAG, "Owing " + debt + " to "
                                                + selectedRecipientID);
                                        currentValue = 0.0;
                                    }
                                }
                                appExecutors.mainThread().execute(() -> {
                                    activityTopupBinding.includedLayout
                                            .currentblanceTxt
                                            .setText(String.valueOf(currentValue));
                                    showToast(getString(R.string.successful_transfer));
                                    activityTopupBinding.includedLayout
                                            .transferAmount.setText("");
                                });
                            });
                        }
                    }
                });
    }

    private void topupUIUpdate() {
        appExecutors.mainThread().execute(() -> {
            Log.d(TAG, "execute main thread");
            String amount = activityTopupBinding.includedLayout.topupAmount.getText().toString();
            if (TextUtils.isEmpty(amount)) {
                TopupActivity.this.showAmountValidationMessage(
                        TopupActivity.this.getString(R.string.invalid_amount),
                        TopupActivity.this.getString(R.string.invalid_amount_msg));
            } else {
                double topup = Double.parseDouble(amount);
                Log.d(TAG, "Topup Amount " + amount);
                if (topup == 0) {
                    TopupActivity.this.showAmountValidationMessage(
                            TopupActivity.this.getString(R.string.invalid_amount),
                            TopupActivity.this.getString(R.string.invalid_amount_msg));
                } else {
                    if (debtAmount > 0) {
                        if (topup < debtAmount) {
                            Log.d(TAG, "great debts...");
                            TopupActivity.this.updateRecord(0.0, debtAmount - topup,
                                    0.0,
                                    userName);
                            activityTopupBinding.includedLayout
                                    .currentblanceTxt.setText(String.valueOf(0.0));
                            activityTopupBinding.includedLayout.topupAmount.setText("");
                            activityTopupBinding.includedLayout.currentblanceTxt
                                    .setText(String.valueOf(0.0));
                            Log.d(TAG, "updated the lbl ");
                            Log.d(TAG, "topup " + topup);
                            Log.d(TAG, "Transferred " + topup + " to " + selectedRecipientID);
                            Log.d(TAG, "Your balance is " + 0.0);
                            Log.d(TAG, "Owning " + (debtAmount - topup)
                                    + " to " + selectedRecipientID);
                            showToast(TopupActivity.this.getString(R.string.pay_debt));
                            getUser(selectedRecipientID, (User user) -> {
                                double currentAmount1 = user.getAmount();
                                TopupActivity.this.updateRecord(currentAmount1 + topup,
                                        selectedRecipientID);
                            });
                        } else {
                            TopupActivity.this.updateRecord(topup - debtAmount, 0.0,
                                    0.0,
                                    userName);
                            activityTopupBinding.includedLayout
                                    .currentblanceTxt.setText(String.valueOf(topup - debtAmount));
                            activityTopupBinding.includedLayout.topupAmount.setText("");
                            Log.d(TAG, "topup " + topup);
                            Log.d(TAG, "Transferred " + debtAmount + " to "
                                    + selectedRecipientID);
                            Log.d(TAG, "Your balance is " + (topup - debtAmount));
                            Log.d(TAG, "Owing " + 0.0 + " to " + selectedRecipientID);

                            TopupActivity.this.showToast(TopupActivity.this.getString(R.string.pay_debt));
                            TopupActivity.this.getUser(selectedRecipientID, user -> {
                                double currentAmount = user.getAmount();
                                updateRecord(currentAmount + debtAmount,
                                        selectedRecipientID);
                            });
                        }
                    } else {
                        double newVal = CommonUtils.getInstance().roundAmount(currentValue + topup);
                        activityTopupBinding.includedLayout.topupAmount.setText("");
                        getUser(userName, user -> {
                            TopupActivity.this.updateRecord(newVal + user.getAmount(),
                                    userName);
                            appExecutors.mainThread().execute(() -> {
                                double tempVal = newVal + user.getAmount();
                                activityTopupBinding.includedLayout
                                        .currentblanceTxt.setText(String.valueOf(tempVal));
                                Log.d(TAG, "topup " + tempVal);
                                Log.d(TAG, "Your balance is " + tempVal);
                            });
                        });
                    }
                }
            }
        });
    }

    private void topupAction() {
        activityTopupBinding.includedLayout.topupBtn.setOnClickListener(v -> {
            Log.d(COMMON_TAG, "onClick: Topup...");
            getUser(userName, user -> {
                debtAmount = user.getDebpt();
                Log.d(TAG, "execute diskIO " + debtAmount);
                topupUIUpdate();
            });
        });
    }

    private void showToast(String message) {
        Toast.makeText(TopupActivity.this, message, Toast.LENGTH_LONG).show();
    }

    private void updateRecipient(double amount) {
        getUser(selectedRecipientID, user -> {
            recipient = user;
            double recipientCurrentAmount = recipient.getAmount();
            updateRecord(recipientCurrentAmount + amount,
                    recipient.getUserName());
        });
    }

    private void updateRecord(double newVal, String userName) {
        appExecutors.diskIO().execute(() -> {
            userInfoViewModel.updateUser(newVal, 0, 0, userName);
            Log.d(COMMON_TAG, "update record output");
        });
    }

    private void updateRecord(double newVal, double debt, double received, String userID) {
        appExecutors.diskIO().execute(() -> {
            userInfoViewModel.updateUser(newVal, debt, received, userID);
            Log.d(COMMON_TAG, "update record output");
        });
    }

    private void getUser(String userID, UpdateListener listener) {
        appExecutors.diskIO().execute(() -> {
            User userLiveData = userInfoViewModel.getUser(userID);
            listener.getUser(userLiveData);
        });
    }

    private void showAmountValidationMessage(String title, String message) {
        appExecutors.mainThread().execute(() -> UiUtils.getInstance()
                .showMaterialDialog(TopupActivity.this,
                        new CustomDialogInterface() {
                            @Override
                            public void ok(DialogInterface dialogInterface) {
                                Log.d(COMMON_TAG, "ok: pressed...");
                                dialogInterface.dismiss();
                            }

                            @Override
                            public void cancel(DialogInterface dialogInterface) {

                            }
                        }, title, message, false));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}